<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use linslin\yii2\curl;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                //'only' => ['logout'],
                'rules' => [
                    [
                        //'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionFeeds()
    {
        $rss = new \DOMDocument();
        $rss->load("https://www.theregister.co.uk/software/headlines.atom");
        $feeds = $feedWords = $array_word_counts = [];
        $commonWords = [
            'time',
            'person',
            'year',
            'way',
            'day',
            'thing',
            'man',
            'world',
            'life',
            'hand',
            'part',
            'child',
            'eye',
            'woman',
            'place',
            'work',
            'week',
            'case',
            'point',
            'government',
            'company',
            'number',
            'group',
            'problem',
            'fact',
            'Verbs',
            'be',
            'have',
            'do',
            'say',
            'get',
            'make',
            'go',
            'know',
            'take',
            'see',
            'come',
            'think',
            'look',
            'want',
            'give',
            'use',
            'find',
            'tell',
            'ask',
            'work',
            'seem',
            'feel',
            'try',
            'leave',
            'call',
            'Adjectives',
            'good',
            'new',
            'first',
            'last',
            'long',
            'great',
            'little',
            'own',
            'other',
            'old',
            'right',
            'big',
            'high',
            'different',
            'small',
            'large',
            'next',
            'early',
            'young',
            'important',
            'few',
            'public',
            'bad',
            'same',
            'able',
            'Prepositions',
            'to',
            'of',
            'in',
            'for',
            'on',
            'with',
            'at',
            'by',
            'from',
            'up',
            'about',
            'into',
            'over',
            'after',
            'Others',
            'the',
            'and',
            'a',
            'that',
            'I',
            'it',
            'not',
            'he',
            'as',
            'you',
            'this',
            'but',
            'his',
            'they',
            'her',
            'she',
            'or',
            'an',
            'will',
            'my',
            'one',
            'all',
            'would',
            'there',
            'their',
            'See also',
        ];
        // Feed data processing
        foreach ($rss->getElementsByTagName('entry') as $node) {
            $item = array(
                'id' => $node->getElementsByTagName('id')->item(0)->nodeValue,
                'title' => strip_tags($node->getElementsByTagName('title')->item(0)->nodeValue),
                'summary' => strip_tags($node->getElementsByTagName('summary')->item(0)->nodeValue),
                'updated' => $node->getElementsByTagName('updated')->item(0)->nodeValue,
                'author' => $node->getElementsByTagName('author')->item(0)->nodeValue,
            );
            array_push($feeds, $item);
            // Special Character remove from feed data
            $feedWords = explode(' ', preg_replace('/(?=[^]*[^A-Za-z \'-.,])([^ ]*)(?:\\s+|$)/', '', strtolower($item['title'] . ' ' . $item['summary'])));
            // Feed data count
            $array_word_counts = array_merge($array_word_counts, array_values(array_diff($feedWords, $commonWords)));
        }
        // Feed data count
        $array_word_counts = array_count_values($array_word_counts);
        // Sorting descending
        arsort($array_word_counts);

        return $this->render('feeds', [
            'feeds' => $feeds,
            'tags' => array_slice($array_word_counts, 0, 10)
        ]);
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
