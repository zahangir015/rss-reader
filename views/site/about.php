<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        This is the About page. You may modify the following file to customize its content:
    </p>

    <code><?= __FILE__ ?></code>

    <h4>New builds, Project Scarlett and much more</h4>
    <p><strong>Roundup</strong>  New builds, a prolonged farewell to old friends and a new toy (for the boss, at least). That's right, it's the past week at Microsoft brought to you in bite-sized chunks by <i>El Reg</i>.…</p>
    tag:theregister.co.uk,2005:story206101 2019-12-09T10:00:13Z Lindsay Clark https://search.theregister.co.uk/?author=Lindsay%20Clark

    <h4>So the decades we invested in the last platform mean nothing?</h4>
    <p>The lure of shiny new things is particularly irrepressible in December as Christmas approaches, but SAP customers seem to be able to resist it.…</p>
    <p><!--#include virtual='/data_centre/_whitepaper_textlinks_top.html' --></p>
    tag:theregister.co.uk,2005:story206109 2019-12-06T21:31:48Z Thomas Claburn https://search.theregister.co.uk/?author=Thomas%20Claburn


</div>
