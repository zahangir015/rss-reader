<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'Feeds';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>
    <div class="row">
        <div style="margin-bottom: 10px">
            <?php foreach ($tags as $tag => $count): ?>
                <button type="button" class="btn btn-primary"><?= $tag ?> <span class="badge"><?= $count ?></span></button>
            <?php endforeach; ?>
        </div>
    </div>
    <div class="row">
        <?php foreach ($feeds as $key => $singleFeed): ?>
            <div class="col-md-12" style=" background: #d3d3d33b; border-radius: 5px; margin-bottom: 10px">
                <h1 class="mt-5"><?= $singleFeed['title'] ?></h1>
                <p class="lead"><?= $singleFeed['summary'] ?></p>
                <p>Author: <?= $singleFeed['author'] ?> <span style="float: right">Updated: <?= $singleFeed['updated'] ?></span></p>
            </div>
        <?php endforeach; ?>
    </div>
</div>
